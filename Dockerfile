FROM alpine:latest as prepare_stage

# Author
LABEL maintainer="maxime1907 <maxime1907.dev@gmail.com>"

RUN apk update \
    && apk upgrade \
    && apk add \
            git \
            bash

RUN mkdir -p /package_plugins_zombie/addons/sourcemod/scripting/plugins/

WORKDIR /home/git/

RUN git clone https://gitlab.com/counterstrikesource/zombiereloaded.git --recursive --depth 1

# ZombieReloaded
WORKDIR /home/git/zombiereloaded

RUN mkdir -p /package_plugins_zombie/addons/sourcemod/scripting/plugins/zombiereloaded/addons/sourcemod/scripting/
RUN bash /home/git/zombiereloaded/updateversion.sh --unofficial
RUN cp /home/git/zombiereloaded/src/* -R /package_plugins_zombie/addons/sourcemod/scripting/plugins/zombiereloaded/addons/sourcemod/scripting/
RUN cp /home/git/zombiereloaded/cstrike/* -R /package_plugins_zombie/

FROM registry.gitlab.com/counterstrikesource/gamemode/base/sourcemod/plugins:latest

# Author
LABEL maintainer="maxime1907 <maxime1907.dev@gmail.com>"

COPY --from=prepare_stage /package_plugins_zombie/ /package_plugins_zombie
RUN cp -R /package_plugins_zombie/* /package_plugins

# Compile plugins
COPY . /package_plugins/addons/sourcemod/scripting/plugins

# Copy include files
RUN cp -R /package_plugins/addons/sourcemod/scripting/plugins/*/addons/sourcemod/scripting/include/* /package_plugins/addons/sourcemod/scripting/plugins/include

# Copy game resources
RUN cp -R /package_plugins/addons/sourcemod/scripting/plugins/*/addons/sourcemod/data/* /package_plugins/addons/sourcemod/data/; echo -n
RUN cp -R /package_plugins/addons/sourcemod/scripting/plugins/*/addons/sourcemod/gamedata/* /package_plugins/addons/sourcemod/gamedata/; echo -n
RUN cp -R /package_plugins/addons/sourcemod/scripting/plugins/*/addons/sourcemod/translations/* /package_plugins/addons/sourcemod/translations/; echo -n
RUN cp -R /package_plugins/addons/sourcemod/scripting/plugins/*/materials/* /package_plugins/materials/; echo -n
RUN cp -R /package_plugins/addons/sourcemod/scripting/plugins/*/models/* /package_plugins/models/; echo -n
RUN cp -R /package_plugins/addons/sourcemod/scripting/plugins/*/sound/* /package_plugins/sound/; echo -n
RUN cp -R /package_plugins/addons/sourcemod/scripting/plugins/*/cfg/* /package_plugins/cfg/; echo -n

RUN python3 compile-all.py

# Copy plugins data, configs and .smx
RUN cp -R /package_plugins/addons/sourcemod/scripting/plugins/plugins/* /package_plugins/addons/sourcemod/plugins/base/
